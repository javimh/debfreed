#Uninstall Script
echo "Removing files..."
rm -rf /home/$USER/.debfreed
rm -rf /home/$USER/.nonfreepkg.log
rm -rf /home/$USER/.contribpkg.log
#Removing from PATH
sed -i '/#Debfreed path/d' /home/$USER/.bashrc
sed -i '/export PATH=$PATH:\/home\/yorozuya\/.debfreed\/source-code/d' /home/$USER/.bashrc
echo "Done!"
